﻿using UnityEngine;
using UnityEngine.UI ;
using System.Collections;
using AR;

namespace AR
{
    public class ChangeWeapon : MonoBehaviour
    {
        //CZAS MIEDZY ZMIANA BRONI
        public float timeSwitch = 0.2f;

        float Switch = 0;

        //CZY POSIADAMY BRON W DANYM SLOCIE
        bool[] WeaponCheck = { false, false, false, false };

        //OBECNIE UZYWANA BRON ORAZ POPRZEDNIA
        GameObject UseWeapon;
        GameObject PrevWeapon;

        //SLOTY NA BRON
        GameObject[] WeaponSlot = { null, null, null, null };

        void Start()
        {

            for (int i = 0; i < 4; i++)
            {
                //SZYKAMY SLOTU Z BRONIA
                GameObject AAA = GameObject.FindGameObjectWithTag("WeaponSlot" + i);
                if (AAA != null)
                {
                    //JESLI MAMY TAKI TO PRZYPISUJEMY GO DO TABLICY
                    WeaponSlot[i] = AAA;
                    //Debug.Log( WeaponSlot[i].name ) ;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                //SPRAWDZAMY CZY W SLOCIE MAMY BRON 
                //( SZUKAMY MoveWeapon KAZDA BRON BEDZIE MIALA TEN SKRYPT )
                MoveWeapon c = WeaponSlot[i].GetComponentInChildren<MoveWeapon>();
                if (c != null)
                {
                    //JESLI MAMY TO TRUE
                    WeaponCheck[i] = true;
                }
                else
                {
                    //JESLI NIE TO WYLACZAMY TAKI SLOT ( ZEBY NIE BYLO GO WIDAC )
                    WeaponSlot[i].SetActive(false);
                    WeaponCheck[i] = false;
                }
            }

            //ZMIANNA DO WYLACZANIA SLOTOW W KTORYCH JUZ MAMY BRON
            bool FindFirstWeapon = false;

            for (int i = 0; i < 4; i++)
            {
                //JESLI W SLOCIE JEST BRON I NIE ZNALEZLISMY JESZCZE 1 POSIADANEJ BRONI
                if (WeaponCheck[i] && !FindFirstWeapon)
                {
                    FindFirstWeapon = true;
                    //USTAWIAMY UZYWANA BRON NA DANY SLOT
                    UseWeapon = WeaponSlot[i];
                    //WLACZAMY SLOT
                    UseWeapon.SetActive(true);
                    //POPRZEDNIA BRON OBECNIE JEST NULL
                    PrevWeapon = null;
                }
                else
                {
                    //JESLI JUZ ZNALEZLISMY 1 POSIADANA BRON A MAMY WIECEJ BRONI 
                    //TO WYLACZAMY TE SLOTY ( ZEBY NIE MIEC 2 LUB WIECEJ BRONI NA EKRANIE )
                    WeaponSlot[i].SetActive(false);
                }
            }

        }


        // Update is called once per frame
        void Update()
        {
            Switch += Time.deltaTime;

            //ZEBY ZMIENIC BRON POTRZEBNY WCISNIETY PRZYCISK / POSIADANA BRON / CZAS MIEDZY ZMIANA
            if (Input.GetKeyDown(KeyCode.Alpha1) && WeaponCheck[0] && Switch > timeSwitch )//&& !UseWeapon.Equals(WeaponCheck[0]))
            {
                Switch = 0;
                //WYLACZAMY OBECNY SLOT
                UseWeapon.SetActive(false);
                //POPRZEDNIA BRON = OBECNEJ
                PrevWeapon = UseWeapon;
                //WYBIERAMY NOWY SLOT DLA OBECNEJ BRONI I WLACZAMY GO
                UseWeapon = WeaponSlot[0];
                UseWeapon.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2) && WeaponCheck[1] && Switch > timeSwitch )//&& !UseWeapon.Equals(WeaponCheck[1]))
            {
                Switch = 0;
                UseWeapon.SetActive(false);

                PrevWeapon = UseWeapon;
                UseWeapon = WeaponSlot[1];
                UseWeapon.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3) && WeaponCheck[2] && Switch > timeSwitch )//&& !UseWeapon.Equals(WeaponCheck[2]))
            {
                Switch = 0;
                UseWeapon.SetActive(false);

                PrevWeapon = UseWeapon;
                UseWeapon = WeaponSlot[2];
                UseWeapon.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.Alpha4) && WeaponCheck[3] && Switch > timeSwitch )//&& !UseWeapon.Equals(WeaponCheck[3]))
            {
                Switch = 0;
                UseWeapon.SetActive(false);

                PrevWeapon = UseWeapon;
                UseWeapon = WeaponSlot[3];
                UseWeapon.SetActive(true);
            }

            //ZMIANA NA POPRZEDNIA BRON
            if (Input.GetKeyDown(KeyCode.Q) && Switch > timeSwitch)
            {
                //JESLI JUZ ZMIENIALISMY BRON ( CZYLI COS JEST W POPRZEDNIEJ BRONI )
                if (PrevWeapon != null)
                {
                    Switch = 0;
                    UseWeapon.SetActive(false);

                    GameObject B = UseWeapon;

                    UseWeapon = PrevWeapon;
                    PrevWeapon = B;
                    UseWeapon.SetActive(true);
                }
            }

        }
    }
}
