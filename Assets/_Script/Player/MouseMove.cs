﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class MouseMove : MonoBehaviour
    {
        public enum Rotation { MouseXandY = 0 , MouseX = 1 , MouseY = 2 };
        public Rotation mouseRotationAxes = Rotation.MouseY;

        public float sensitivityX = 15F;
        public float sensitivityY = 15F;

        public float minimumAngleX = -360F;
        public float maximumAngleX = 360F;

        public float minimumAngleY = -60F;
        public float maximumAngleY = 60F;

        public float rotationY = 0F;
        public float rotationX = 0f;


        float odlRotationY;
        float differenceRotationY;

        public float moveMouseDifferenceRotationY()
        {
            return differenceRotationY;
        }

        // Use this for initialization
        void Start()
        {
            odlRotationY = 0;
            differenceRotationY = 0;
            //zapobiega rotacji za pomoca Rigidbody
            if (GetComponent<Rigidbody>())
                GetComponent<Rigidbody>().freezeRotation = true;
        }

        // Update is called once per frame
        void Update()
        {
            switch(mouseRotationAxes)
            {
                case Rotation.MouseY:

                    odlRotationY = rotationY;

                    rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                    rotationY = Mathf.Clamp(rotationY, minimumAngleY, maximumAngleY);

                    transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);

                    differenceRotationY = odlRotationY - rotationY;

                    break;

                case Rotation.MouseX:

                    rotationX = Input.GetAxis("Mouse X") * sensitivityX;
                    transform.Rotate(0, rotationX, 0);

                    break;

                case Rotation.MouseXandY:

                    rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

                    rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                    rotationY = Mathf.Clamp(rotationY, minimumAngleY, maximumAngleY);

                    transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);

                    break;

                default:
                    break;
            }
        }
    }
}