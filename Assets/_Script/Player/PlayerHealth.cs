﻿using UnityEngine;
using UnityEngine.UI ;
using System.Collections;
using AR;

namespace AR
{
    public class PlayerHealth : MonoBehaviour
    {
        //ILE ZYCIA BEDZIE MIAL GRACZ
        public int playerHealth = 100;
        //DZWIEK KIEDY GRACZ UMIERA
        //public AudioClip playerDeath ;


        //SZYBKOSC ZNIKNIECIA DamageImage
        public float fadeDamageImage = 5f;
        //KOLOR MIGOTANIA					RED , GREEN , BLUE , ALPHA
        public Color DamageColor = new Color(1f, 0f, 0f, 0.1f);
        //ZDJECIA DAMAGE (MIGAJACY CZERWONY)
        Image DamageImage;
        //TEXT ZYCIA
        Text HPtext;

        //ANIMACJA POSTACI
        //Animator animationPlayer ;
        //DZWIEK JAKI "WYDOBYWA" SIE Z GRACZA (OBECNIE)
        //AudioSource playerAudio ;

        //OBECNE ZYCIE GRACZA
        int currentHealthPlayer;

        //ZMIENNE BOOL DO SMIERCI LUB DAMAGE GRACZA
        bool PlayerDead;
        bool damage;

        void Awake()
        {
            //PRZYPISUJEMY KOMPONENTY ZMIENNYM
            /*
            animationPlayer = GetComponent<Animator>() ;
            playerAudio = GetComponent<AudioSource>() ;
            playerMove = GetComponent<PlayerMove>() ;
             */

            //SZUKAMY TEKSTU HP
            GameObject HPObject = GameObject.FindGameObjectWithTag("HPText");
            //SZYKAMY DAMAGE IMAGE
            GameObject DamageObject = GameObject.FindGameObjectWithTag("DamageImage");

            //SPRAWDZAMY CZY ZNALAZL HPObject
            if (HPObject == null)
            {
                Debug.Log(" HPObject   PlayerHealth ");
            }
            else
                HPtext = HPObject.GetComponent<Text>();

            //SPRAWDZAMY CZY ZNALAZL DamageObject
            if (DamageObject == null)
            {
                Debug.Log(" DamageObject   PlayerHealth ");
            }
            else
                DamageImage = DamageObject.GetComponent<Image>();


            //USTAWIAMY POCZATKOWE ZYCIE GRACZA
            currentHealthPlayer = playerHealth;
        }


        // Update is called once per frame
        void Update()
        {
            if (DamageImage != null)
            {
                //JESLI GRACZ OTRZYMAL OBRAZENIA
                if (damage)
                {
                    //USTAWIAMY KOLOR MIGNIECIA NA EKRANIE
                    DamageImage.color = DamageColor;
                }
                else
                {
                    //DZIEKI LERP ZMIENIAMY KOLOR Z OBECNEGO NA CZYSTY (NIEWIDOCZNY) 
                    DamageImage.color = Color.Lerp(DamageImage.color, Color.clear, fadeDamageImage * Time.deltaTime);
                }
            }

            damage = false;

            if (HPtext != null)
            {
                //USTAWIAMY OBECNY STAN ZYCIA
                if (currentHealthPlayer > 0)
                    HPtext.text = "HP " + currentHealthPlayer;
                else
                    HPtext.text = "HP 0";
            }
        }


        //FUNKCJA UZYWANA PRZEZ INNE OBIEKTY ZEBY ZADAC OBRARZENIA GRACZOWI
        public void TakeDamage(int HowMuch)
        {
            //OTRZYMUJEMY OBRAZENIA
            damage = true;

            //Debug.Log( "Get Damage" ) ;

            //ODTWARZAMY DZWIEK OBRAZEN
            //playerAudio.Play () ;

            //ODEJMUJEMY OD ZYCIA OBRAZENIA
            currentHealthPlayer -= HowMuch;

            //SPRAWDZAMY CZY GRACZ ZYJE ORAZ NIE JEST JESZCZE MARTWY
            /*
            if( currentHealthPlayer <= 0 && !PlayerDead )
            {
                HeIsDeath() ;
            }
            */
        }


        /*public void AddHealth( int addHealth )
        {
            //DODAJEMY ZYCIE
            currentHealthPlayer += addHealth ;
            //SPRAWDZAMY CZY NIE PRZEKROCZYLISMY MAX ILOSC ZYCIA JESLI TAK TO ZMNIEJSZAMY DO MAX
            if( currentHealthPlayer > playerHealth )
                currentHealthPlayer = playerHealth ;

            //USTAWIAMY OBECNY STAN NA HealthBar
            healthBar.value = currentHealthPlayer ;
        }*/

        public int CurrentPlayerHealth()
        {
            //Debug.Log( "Curent Health" ) ;
            return currentHealthPlayer;
        }


        /*void HeIsDeath()
        {
            //GRACZ NIE ZYJE
            PlayerDead = true ;

            //USTAWIAMY ZMIENNA ANIMACJI NA TRUE
            animationPlayer.SetTrigger ("Die") ;

            //USTAWIAMY NOWY DZWIEK (SMIERC GRACZA) PO CZYM ODPALAMY GO
            playerAudio.clip = playerDeath ;
            playerAudio.Play() ;

            //POBIERAMY SKRYPT BRONI ( BO MOGLA SIE ZMIENIC )
    //		playerShoot = GetComponentInChildren<PlayerShoot>() ;

            //WYLACZAMY SKRYPT PORUSZANIA SIE GRACZA ORAZ STRZELANIA ( SKORO NIE ZYJE TO LEPIEJ ZEBY SIE NIE RUSZAL I NIE STRZELAL )
            if ( playerMove.enabled )
                playerMove.enabled = false ;
    //		if ( playerShoot.enabled )
    //			playerShoot.enabled = false ;
        }*/
    }
}
