﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class Inventory : MonoBehaviour
    {
        //MAX AMUNICJI JAKI MOZEMY NOSIC
        public int maxAmmoRifle = 270;
        public int maxAmmoShootgun = 80;
        public int maxAmmoPistol = 100;

        int currentAmmoM4;
        int currentAmmoAk47;

        void Start()
        {
            currentAmmoM4 = maxAmmoRifle;
            currentAmmoAk47 = maxAmmoRifle;
        }

        public int CurrentAmmoM4()
        {
            return currentAmmoM4;
        }

        public int CurrentAmmoAk47()
        {
            return currentAmmoAk47;
        }


        public int ReloadM4(int ammoMag, int currentAmmo)
        {
            //JESLI MAMY COS NABOI
            if (currentAmmoM4 > 0)
            {
                //SPRAWDZAMY ILE BRAKUJE NABOI
                int ammo = ammoMag - currentAmmo;

                if (currentAmmoM4 >= ammo)
                {
                    //JESLI MAMY TYLE NABOI W PLECAKU ILE TRZEBA DO PELNEGO MAGAZYNKA
                    //TO ODEJMUJEMY OD OBECNEJ ILOSCI NABOI W PLECAKU
                    //I ZWRACAMY LICZBE NABOI
                    currentAmmoM4 -= ammo;
                    return ammo;
                }
                else
                {
                    //JESLI NIE MAMY TYLE ILE POTRZEBA TO OBECNA ILOSC NABOI ZERUJEMY
                    //I ZWRACAMY TYLE ILE MOZEMY
                    ammo = currentAmmoM4;
                    currentAmmoM4 = 0;
                    return ammo;
                }
            }
            else
            {
                //JESLI NIE MAMY NABOI TO ZWRACAMY 0 ( TAK NA WSZELKI TYPADEK )
                return 0;
            }
        }

        public int ReloadAk47(int ammoMag, int currentAmmo)
        {
            //JESLI MAMY COS NABOI
            if (currentAmmoAk47 > 0)
            {
                //SPRAWDZAMY ILE BRAKUJE NABOI
                int ammo = ammoMag - currentAmmo;

                if (currentAmmoAk47 >= ammo)
                {
                    //JESLI MAMY TYLE NABOI W PLECAKU ILE TRZEBA DO PELNEGO MAGAZYNKA
                    //TO ODEJMUJEMY OD OBECNEJ ILOSCI NABOI W PLECAKU
                    //I ZWRACAMY LICZBE NABOI
                    currentAmmoAk47 -= ammo;
                    return ammo;
                }
                else
                {
                    //JESLI NIE MAMY TYLE ILE POTRZEBA TO OBECNA ILOSC NABOI ZERUJEMY
                    //I ZWRACAMY TYLE ILE MOZEMY
                    ammo = currentAmmoAk47;
                    currentAmmoAk47 = 0;
                    return ammo;
                }
            }
            else
            {
                //JESLI NIE MAMY NABOI TO ZWRACAMY 0 ( TAK NA WSZELKI TYPADEK )
                return 0;
            }
        }


    }
}
