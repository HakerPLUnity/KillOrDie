﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class CrouchCollider : MonoBehaviour
    {
        public float distanceUP = 0.1f;
        //ILE MA SE UNOSIC DODATKOWO NAD GRACZEM
        //float floatAboveHead = 0.1f  ;
        //KONTROLER DLA WYSOKOSCI KAPSULY
        CharacterController heightCollider;
        GameObject playerPosition;
        [HideInInspector]
        public bool ifCollision = false;


        Vector3[] pointRaycast;


        public bool isCollision()
        {
            return ifCollision;
        }

        void Awake()
        {
            pointRaycast = new Vector3[9];
            //POBIERAMY OJCA OBIEKTU
            playerPosition = transform.parent.gameObject;
            if (!playerPosition)
                Debug.Log("CrouchCollider playerPosition");
            else
            {
                heightCollider = playerPosition.GetComponent<CharacterController>();
            }

            if (!heightCollider)
            {
                Debug.Log("CrouchCollider heightCollider");
            }

        }

        // Update is called once per frame
        void Update()
        {
            transform.position = new Vector3(playerPosition.transform.position.x,
                                             //POBIERAMY WYSOKOSC KAPSULY I AKTUALNE SKALOWANIE
                                             playerPosition.transform.position.y + heightCollider.center.y + (heightCollider.height / 2)
                                            //((heightCollider.height  * playerPosition.transform.localScale.y)) 
                                            //DO TEGO DODAJEMY POLOWE WIELKOSCI + ILOSC JAKA MA SIE UNOSIC NAD GLOWA
                                            //BEZ TEGO KULA BEDZIE NA WYSOKOSCI GLOWY
                                            //( playerPosition.transform.localScale.y / 2f ) + floatAboveHead   ,
                                            , playerPosition.transform.position.z);




            //lewy gorny
            pointRaycast[0] = new Vector3(transform.position.x - heightCollider.radius, transform.position.y, transform.position.z + heightCollider.radius);
            //srodek gorny
            pointRaycast[1] = new Vector3(transform.position.x, transform.position.y, transform.position.z + heightCollider.radius);
            //prawy gorny
            pointRaycast[2] = new Vector3(transform.position.x + heightCollider.radius, transform.position.y, transform.position.z + heightCollider.radius);

            //lewy srodek
            pointRaycast[3] = new Vector3(transform.position.x - heightCollider.radius, transform.position.y, transform.position.z);
            //srodek
            pointRaycast[4] = transform.position;
            //prawy srodek
            pointRaycast[5] = new Vector3(transform.position.x + heightCollider.radius, transform.position.y, transform.position.z);

            //lewy dolny
            pointRaycast[6] = new Vector3(transform.position.x - heightCollider.radius, transform.position.y, transform.position.z - heightCollider.radius);
            //srodek dolny
            pointRaycast[7] = new Vector3(transform.position.x, transform.position.y, transform.position.z - heightCollider.radius);
            //prawy dolny
            pointRaycast[8] = new Vector3(transform.position.x + heightCollider.radius, transform.position.y, transform.position.z - heightCollider.radius);

            foreach (Vector3 position in pointRaycast)
            {
                if(this.checkRaycst(position))
                {
                    ifCollision = true;
                    return;
                }
            }

            ifCollision = false;
        }

        /*void OnCollisionStay(Collision other)
        {
            if (other.gameObject.layer == 30)
            {
                //Debug.Log( "Collision MAP" ) ;
                ifCollision = true;
            }
        }

        void OnCollisionExit()
        {
            //Debug.Log( "Collision MAP" ) ;
            ifCollision = false;
        }*/

        private bool checkRaycst(Vector3 positionRay)
        {
            RaycastHit hit;
            if (Physics.Raycast(positionRay, Vector3.up, out hit, distanceUP))
            {
                //print("test111");
                return true;
            }
            return false;
        }
    }
}
