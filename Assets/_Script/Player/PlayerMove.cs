﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class PlayerMove : MonoBehaviour
    {
        //SZYBKOSC CHODZENIA
        public float SpeedForward = 20f;
        public float SpeedBack = 10f;
        public float SpeedSide = 15f;
        //SZYBKOSC (WYSOKOSC) SKOKU
        public float jumpSpeed = 15f;
        //GRAVITACJA
        public float gravity = 0.8f;

        //CZY JESTESMY NA ZIEMI
        //bool ground = false ;
        //KIERUNEK PORUSZANIA SIE = ZERO
        Vector3 MoveDirection = Vector3.zero;
        //KOMPONENT GRACZA 
        CharacterController Controler;
        //FLAGA (POTRZEBNE DO TEGO CZY JESTESMY NA ZIEMI)
        CollisionFlags flags;

        //KIERUNEK PORUSZANIA SIE
        float z;
        float x;

        float crouchHeight;
        float standHeight;
        Transform cameraChild;
        float toPoseStatus;
        float actualPoseStatus;
        float fastCrouch;
        float timeToCrouch = 0.1f;
        CrouchCollider sphereCollider;

        void Awake()
        {
            toPoseStatus = 1f;
            actualPoseStatus = 1f;
            //POBIERAMY KOMPONENT I WYSOKOSC
            Controler = GetComponent<CharacterController>();
            //WIELKOSC KUCAJACEJ POSTACI
            crouchHeight = Controler.height / 1.8f;
            //WIELKOSC STOJACEJ POSTACI
            standHeight = Controler.height;
            //OBIEKT KTORY MA BYC NIE SKALOWANY 
            //KAMERA , BRON , OBIEKT RECOIL
            cameraChild = transform.Find("CamGunRecoil");

            sphereCollider = gameObject.GetComponentInChildren<CrouchCollider>();
        }


        // Update is called once per frame
        void FixedUpdate()
        {
            //JESLI JESTESMY NA ZIEMI MOZEMY SIE PORUSZAC
            if (Controler.isGrounded)
            {
                //POBIERAMY KIERUNEK
                z = Input.GetAxis("Vertical");
                x = Input.GetAxis("Horizontal");

                MoveDirection = new Vector3(x, 0f, z);

                MoveDirection = transform.TransformDirection(MoveDirection);

                //ZMIENIAMY PREDKOSC
                //DO TYLU
                if (z < 0)
                    MoveDirection *= SpeedBack;
                //DO PRZODU
                else if (z > 0)
                    MoveDirection *= SpeedForward;
                //NA BOKI
                else if (x != 0)
                    MoveDirection *= SpeedSide;

                //ZMNIEJSZAMY WYSOKOSC (GRAWITACJA) zeby nie podskakiwac na pochylni
                MoveDirection.y -= gravity * 4f;

                //SKOK
                if (Input.GetKeyDown(KeyCode.Space))
                    MoveDirection.y = jumpSpeed;
            }

            //ZMNIEJSZAMY WYSOKOSC (GRAWITACJA)
            MoveDirection.y -= gravity;


            Controler.height = Mathf.Lerp(crouchHeight, standHeight, actualPoseStatus);
            float currentCenterControler = Mathf.Lerp(-0.45f, 0f, actualPoseStatus);
            Controler.center = new Vector3(0f, currentCenterControler, 0f);
            float currentCameraPosition = Mathf.Lerp(crouchHeight - standHeight, 0f, actualPoseStatus);

            cameraChild.localPosition = new Vector3(cameraChild.localPosition.x, currentCameraPosition, cameraChild.localPosition.z);

            //KUCANIE JESLI PRZYCISK ZOSTANIE WCISNIETY TO KUCAMY 
            //A JAK PUSZCZONY TO WSTAJEMY
            if (Input.GetKey(KeyCode.LeftControl))
            {
                toPoseStatus = 0f;
            }
            else if (!Input.GetKey(KeyCode.LeftControl) && !sphereCollider.ifCollision)
            {
                toPoseStatus = 1f;
            }

            actualPoseStatus = Mathf.SmoothDamp(actualPoseStatus, toPoseStatus, ref fastCrouch, timeToCrouch);

            //POBIERAMY FLAGE
            /*flags =*/
            Controler.Move(MoveDirection * Time.deltaTime);
            //SPRAWDZAMY CZY JESTESMY NA ZIEMI
            //ground = ( flags & CollisionFlags.CollidedBelow ) != 0 ;
        }
    }
}