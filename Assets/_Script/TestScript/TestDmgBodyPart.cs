﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AR;

namespace AR
{
    public class TestDmgBodyPart : MonoBehaviour
    {
        public float multiplierDmg = 1f;

        private Text dmgText;

        protected void Awake()
        {
            dmgText = GameObject.FindGameObjectWithTag("dmgTestText").GetComponent<Text>();
        }
        
        public void Damage(int weaponDmg)
        {
            dmgText.text = "Target name            : " + gameObject.transform.name + "\n" +
                           "Damage weapon      : " + weaponDmg + "\n" +
                           "Damage multiplier    : " + multiplierDmg + "\n" +
                           "Damage to target     : " + (int)(weaponDmg * multiplierDmg);
        }
    }
}
