﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AR;

namespace AR
{
    public static class FindComponentBy
    {
        public static T FindGameObjectInChildByTag<T>(this GameObject parent, string tag) where T : Component
        {
            T component;
            Transform t = parent.transform;
            foreach (Transform tr in t)
            {
                if (tr.tag == tag)
                {
                    return tr.gameObject.GetComponent<T>();
                }
                component = tr.gameObject.FindGameObjectInChildByTag<T>(tag);
                if (component != null)
                {
                    return component.gameObject.GetComponent<T>();
                }
            }
            return null;
        }

        public static T FindGameObjectInChildByName<T>(this GameObject parent, string name) where T : Component
        {
            T component;
            Transform t = parent.transform;
            foreach (Transform tr in t)
            {
                if (tr.name == name)
                {
                    return tr.gameObject.GetComponent<T>();
                }
                component = tr.gameObject.FindGameObjectInChildByTag<T>(name);
                if (component != null)
                {
                    return component.gameObject.GetComponent<T>();
                }
            }
            return null;
        }
    }
}
