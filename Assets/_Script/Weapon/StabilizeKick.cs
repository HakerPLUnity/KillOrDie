﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class StabilizeKick : MonoBehaviour
    {
        public MouseMove Camera;
        public float returnBackSpeed = 2;

        void LateUpdate()
        {
            /*if(Camera.moveMouseDifferenceRotationY() != 0)
            {
                 transform.localRotation = Camera.transform.localRotation;
                    //Quaternion.Euler(transform.localRotation.eulerAngles - new Vector3(Cammera.moveMouseDifferenceRotationY(), 0, 0));   
            }*/
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.identity, Time.deltaTime * returnBackSpeed);
        }
    }
}
