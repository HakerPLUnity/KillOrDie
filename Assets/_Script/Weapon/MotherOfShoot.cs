﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class MotherOfShoot : MonoBehaviour
    {
        //CZY AKTUALNIE STRZELAMY
        [HideInInspector]
        public static bool shootCrossHair = false;
        [HideInInspector]
        public static bool shootRecoil = false;
    }
}
