﻿using UnityEngine;
using UnityEngine.UI ;
using System.Collections;
using AR;

namespace AR
{
    public class Ammo_From_Inventory : MonoBehaviour
    {
        public enum weapon { Ak47, M4 };
        //ILE MOZEMY MIEC MAX NABOI W MAGAZYNKU
        public int sizeMag = 30;
        //TYP BRONI
        public weapon Weapon_Type;

        //TEKST ILOSCI NABOI
        Text AmmoText;

        //ILE MAMY OBECNIE NABOI W MAGAZYNKU
        int currentAmmo;

        //INVENTORY GRACZA
        Inventory inventory;

        // Use this for initialization
        void Start()
        {
            //SZUKAMY OBIEKTU GRACZA
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            //SZUKAMY TEKSTU W CANVAS
            GameObject AmmoObject = GameObject.FindGameObjectWithTag("ammoText");

            if (player != null)
            {
                //POBIERAMY KOMPONENT INVENTORY
                inventory = player.GetComponent<Inventory>();
            }

            if (inventory == null)
            {
                Debug.Log(" inventory is empty ( Ammo_From_Inventory ) ");
            }

            if (AmmoObject == null)
            {
                Debug.Log(" AmmoObject is empty ( Ammo_From_Inventory ) ");
            }
            else
            {
                AmmoText = AmmoObject.GetComponent<Text>();
                if (AmmoText == null)
                {
                    Debug.Log(" AmmoText is empty ( Ammo_From_Inventory ) ");
                }
            }

            //NA POCZATKU ZAWSZE MAMY PELNY MAGAZYNEK
            currentAmmo = sizeMag;
        }

        void Update()
        {
            if (AmmoText != null)
            {
                AmmoText.text = currentAmmo + " / " + AmmoInBag();
            }
        }

        public int AmmoInMag()
        {
            return currentAmmo;
        }

        public void Shoot()
        {
            currentAmmo--;
        }


        public int AmmoInBag()
        {
            //AMMO W PLECAKU
            int ammoBag = 0;

            switch (Weapon_Type)
            {
                case weapon.Ak47:

                    ammoBag = inventory.CurrentAmmoAk47();

                    break;

                case weapon.M4:

                    ammoBag = inventory.CurrentAmmoM4();

                    break;
            }

            return ammoBag;
        }

        public void Reload()
        {
            switch (Weapon_Type)
            {
                case weapon.Ak47:

                    //SPRAWDZAMY CZY MAMY JAKIES NABOJE W PLECAKU
                    if (inventory.CurrentAmmoAk47() > 0)
                    {
                        //LADUJEMY BRON
                        currentAmmo += inventory.ReloadAk47(sizeMag, currentAmmo);
                    }

                    break;

                case weapon.M4:

                    //SPRAWDZAMY CZY MAMY JAKIES NABOJE W PLECAKU
                    if (inventory.CurrentAmmoM4() > 0)
                    {
                        //LADUJEMY BRON
                        currentAmmo += inventory.ReloadM4(sizeMag, currentAmmo);
                    }

                    break;
            }
        }

    }
}
