﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class GunRecoil : MonoBehaviour
    {
        //MAX ODCHYLENIE BRONI PODCZAS STRZELANIA ORAZ SZYBKOSC
        public float recoilMax = -10f;
        public float recoilSpeed = 10f;

        //OBIEKT DO "PORUSZANIA" BRONI PODCZAS STRZALU
        Transform RecoilObject;

        //RECOIL BRONI
        float recoil = 0f;

        void Awake()
        {
            GameObject RecoilPosition = GameObject.FindGameObjectWithTag("RecoilGun");
            if (RecoilPosition)
                RecoilObject = RecoilPosition.transform;
        }


        // Update is called once per frame
        void Update()
        {
            if (MotherOfShoot.shootRecoil)
            {
                recoil += 0.1f;
            }

            WeaponRecoil();
        }


        void WeaponRecoil()
        {

            if (recoil > 0)
            {
                //ZWRACA ROTACJE (KONTOWA ??)
                Quaternion maxRecoil = Quaternion.Euler(recoilMax, 0, 0);

                //ROTACJA PRZEJSCIE Z DO PRZEZ OKRESLONY CZAS
                RecoilObject.rotation = Quaternion.Slerp(RecoilObject.rotation, maxRecoil, Time.deltaTime * recoilSpeed);

                //ZMIENIAMY RORACJE BRONI
                transform.localEulerAngles = new Vector3(RecoilObject.localEulerAngles.x,
                                                         transform.localEulerAngles.y, transform.localEulerAngles.z);

                //ZMNIEJSZAMY RECOIL
                recoil -= Time.deltaTime;
            }
            else
            {
                recoil = 0;

                //ZMIENIAMY RECOIL NA 0
                Quaternion minRecoil = Quaternion.Euler(0, 0, 0);
                //ROTACJA PRZEJSCIE Z DO PRZEZ OKRESLONY CZAS
                RecoilObject.rotation = Quaternion.Slerp(RecoilObject.rotation, minRecoil, Time.deltaTime * recoilSpeed);

                //ZMIENIAMY RORACJE BRONI
                transform.localEulerAngles = new Vector3(RecoilObject.localEulerAngles.x,
                                                         transform.localEulerAngles.y, transform.localEulerAngles.z);
            }
        }
    }
}
