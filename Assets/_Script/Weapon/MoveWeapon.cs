﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class MoveWeapon : MonoBehaviour
    {
        //O ILE MA SIE PRZEMIESCIC BRON
        public float MoveAmount = 1f;
        //SZYBKOSC PRZEMIESZCZENIA
        public float MoveSpeed = 2f;
        //ODLEGLOSC DLA KOLYSANIA BRONI
        public Vector3 distance;
        //SZYBKOSC PORUSZANIA SIE
        public float speedGunMoveLR = 1f;

        //KTORA POZYCJA MA ZOSTAC WYBRANA
        int position = 1;
        //POZYCJE DLA KOLYSANIA
        Vector3 position1;
        Vector3 position2;

        //public GameObject Gun ;
        //ZMIENNE X ORAZ Y DLA VEKTORA NOWEJ POZYCJI
        float PositionX;
        float PositionY;

        //DOMYSLNA POZYCJA BRONI ORAZ NOWA POZYCJA
        Vector3 DefPosition;
        Vector3 NewPosition;


        // Use this for initialization
        void Start()
        {
            //POBIERAMY POZYCJE BRONI
            //LOCAL CZYLI LOKALNA POZYCJA (OBIEKT JEST W SRODKU SWIATA)
            DefPosition = transform.localPosition;

            position1 = DefPosition - distance;
            position2 = DefPosition + distance;
        }

        // Update is called once per frame
        void Update()
        {
            //POBIERAMY ZMIENNE DO NOWEJ POZYCJI ( MYSZKA * ILOSC PRZEMIESZCZENIA * CZAS )
            //MYSZKA - KIERUNEK // ILOSC - O ILE MA SIE PRZEMIESCIC?? // CZAS DLA PLYNNEGO PRZEJSCIA
            PositionX = Input.GetAxis("Mouse X") * MoveAmount * Time.deltaTime;
            PositionY = Input.GetAxis("Mouse Y") * MoveAmount * Time.deltaTime;

            if ((PositionX != 0 || PositionY != 0 || Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0) &&
                     !MotherOfShoot.shootCrossHair)
            {
                if (PositionX != 0 || PositionY != 0)
                {
                    mouseLook(PositionX, PositionY);
                }

                if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
                {
                    gunMove();
                }
            }
            else
                gunBack();
        }

        void gunMove()
        {
            if (position == 1)
            {
                if (Vector3.Distance(transform.localPosition, position1) >= 0.1)
                {
                    transform.localPosition = Vector3.Lerp(transform.localPosition, position1, speedGunMoveLR * Time.deltaTime);
                }
                else
                    position = 2;
            }

            if (position == 2)
            {
                if (Vector3.Distance(transform.localPosition, position2) >= 0.1)
                {
                    transform.localPosition = Vector3.Lerp(transform.localPosition, position2, speedGunMoveLR * Time.deltaTime);
                }
                else
                    position = 1;
            }
        }

        void gunBack()
        {
            if (transform.localPosition != DefPosition)
                transform.localPosition = Vector3.Lerp(transform.localPosition, DefPosition, speedGunMoveLR * Time.deltaTime);
        }

        void mouseLook(float x, float y)
        {
            //USTALAMY NOWA POZYCJE ( PODSTAWOWA + ZMIENNE OD MYSZKI )
            NewPosition = new Vector3(DefPosition.x + x,
                                      DefPosition.y + y,
                                      DefPosition.z);

            //USTAWIAMY PRZEJSCIE Z OBECNEJ POZYCJI DO NOWEJ PLYNNIE ( VECTOR3 NIE MATHF )
            transform.localPosition = Vector3.Lerp(transform.localPosition, NewPosition, MoveSpeed * Time.deltaTime);
        }
    }
}
