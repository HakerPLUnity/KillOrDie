﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class OnOffShoot : MonoBehaviour
    {
        //BRON DO WYLACZENIA
        public MotherOfShoot enableScript;

        public void ShootOn()
        {
            enableScript.enabled = true;
        }
        public void ShootOff()
        {
            enableScript.enabled = false;
        }
    }
}
