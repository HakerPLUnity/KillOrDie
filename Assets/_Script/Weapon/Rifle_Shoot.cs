﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{

    //tablica z vectror3 lub 2 + parametr dla if 
    //np if y > 4 idz w prawo potem if w prawo < 1 idzi w gore itp
    







    public class Rifle_Shoot : MonoBehaviour
    {
        [Header("Bullet Data")]
        public GameObject bullet;

        public float speed = 100;
        public float force = 100;
        private int shotFromObject;

        [Header("Weapon Data")]
        public GameObject shotPosition;
        //OBRAZENIA
        public int DamageShoot = 20;
        //CZAS MIEDZY STRZALAMI
        public float TimeBetweenShoot = 0.15f;
        //ODLEGLOSC NA JAKA STRZELA BRON
        public float range = 100f;

        [Header("Kick Weapon")]
        public GameObject kickWeapon;
        public float kickUP = 0.5f;
        public float kickSideway = 0.2f;

        [Header("Accuracy")]
        //public MouseMove moveMouse;
        public float baseUPAccuracy = 0.5f;
        public float maxUPAccuracy = 2f;
        public float increaseUPAccuracy = 0.2f;
        public float decreaseUPAccuracy = 0.05f;

        public float baseSidewayAccuracy = 1.5f;
        public float maxSidewayAccuracy = 5f;
        public float increaseSidewayAccuracy = 0.5f;
        public float decreaseSidewayAccuracy = 0.2f;
        private float actualUPAccuracy;
        private float actualSidewayAccuracy;

        //CZAS POTRZEBNY DO ODMIEZENIA STRZALU
        float TimerShoot;
        //ZMIANA SHOOT NA FALSE
        float TimeShootTrue;

        //POLACZENIE PLECAKA Z BRONIA
        Ammo_From_Inventory ammo;

        //KOMPONENTY ODPOWIEDZIALNE ZA SWIATLO , DZWIEK , "OBRAZEK" STRZALU , RYSOWANIE LINI
        Light gunLight;
        AudioSource gunAudio;
        ParticleSystem gunParticle;

        //PRZEZ JAKI CZAS EFEKTY(TE WYZEJ) MAJA BYC WYSWIELTANE
        float DiplayTimeEfect = 0.3f;

        void Awake()
        {
            //jaki obiekt wystrzelil pocisk (zapobiega ranieniu siebie samego pociskiem)
            shotFromObject = GetComponentInParent<PlayerHealth>().transform.GetInstanceID();
        }

        void Start()
        {
            //POBIERAMY OD DZIECKA KOMPONENTY WYSTRZALU
            gunLight = GetComponentInChildren<Light>();
            gunAudio = GetComponentInChildren<AudioSource>();
            gunParticle = GetComponentInChildren<ParticleSystem>();

            //POBIERAMY "MAGAZYNEK"
            ammo = GetComponent<Ammo_From_Inventory>();

            if (!gunLight)
                Debug.Log("gunLight is empty  Rifle_Shoot");
            if (!gunAudio)
                Debug.Log("gunAudio is empty  Rifle_Shoot");
            if (!gunParticle)
                Debug.Log("gunParticle is empty  Rifle_Shoot");
            if (!ammo)
                Debug.Log("ammo is empty  Rifle_Shoot");

            actualUPAccuracy = baseUPAccuracy;
            actualSidewayAccuracy = baseSidewayAccuracy;
        }

        // Update is called once per frame
        void Update()
        {
            //USTAWIAMY CZAS
            TimerShoot += Time.deltaTime;
            TimeShootTrue += Time.deltaTime;

            //SPRAWDZAMY CZY CZAS MIEDZY STRZALAMI JEST MNIEJSZY NIZ CZAS ORAZ CZY WCISNIETO PRZYCISK STRZALU
            if (Input.GetMouseButton(0) && TimerShoot >= TimeBetweenShoot && ammo.AmmoInMag() > 0)
            {
                //ODEJMUJEMY AMUNICJE Z MAGAZYNKA
                ammo.Shoot();
                //STRZELAMY
                Shoot();

                //zmniejszamy celnosc broni
                actualUPAccuracy += increaseUPAccuracy;
                actualSidewayAccuracy += increaseSidewayAccuracy;
                if (actualUPAccuracy > maxUPAccuracy)
                {
                    actualUPAccuracy = maxUPAccuracy;
                }
                if (actualSidewayAccuracy > maxSidewayAccuracy)
                {
                    actualSidewayAccuracy = maxSidewayAccuracy;
                }

                //ODPALAMY KOMPONENTY WYSTRZALU
                gunAudio.Play();
                gunParticle.Stop();
                gunParticle.Play();
                gunLight.enabled = true;

            }

            //PRZELADOWANIE
            if (Input.GetKeyDown(KeyCode.R))
            {
                ammo.Reload();
            }

            //JESLI CZAS STRZALU JEST WIEKSZY NIZ CZAS MIEDZY STRZALEM + 0.01 
            //TO ZNACZY ZE NIE STRZELAMY OBECNIE
            if (TimeShootTrue >= TimeBetweenShoot + 0.01f)
            {
                //zwiekszamy celnosc broni
                actualUPAccuracy -= decreaseUPAccuracy;
                actualSidewayAccuracy -= decreaseSidewayAccuracy;
                if (actualUPAccuracy < baseUPAccuracy)
                {
                    actualUPAccuracy = baseUPAccuracy;
                }
                if (actualSidewayAccuracy < baseSidewayAccuracy)
                {
                    actualSidewayAccuracy = baseSidewayAccuracy;
                }
            }

            //SPRAWDZAMY JAK DLUGO SA WYSWIETLANE EFEKTY I JESLI SA DLUZEJ WYSWIETLANE TO JE WYLACZAMY
            if (TimerShoot >= TimeBetweenShoot * DiplayTimeEfect)
            {
                //WYLACZAMY EFEKTY
                DisableEfect();
            }
        }


        public void DisableEfect()
        {
            //WYLACZAMY KOMPONENTY
            gunLight.enabled = false;
        }


        void Shoot()
        {
            //ZERUJEMY CZAS
            TimerShoot = 0F;
            TimeShootTrue = 0f;
            
            GameObject bulletPrefab = ((GameObject)Instantiate(bullet, shotPosition.transform.position, shotPosition.transform.rotation));
            Vector2 direction = new Vector2((Random.Range(-1f, 1f) * actualSidewayAccuracy), (Random.Range(-1f, 1f) * actualUPAccuracy));

            bulletPrefab.GetComponent<Bullet>().updateData(direction, speed, force, DamageShoot, shotFromObject);

            /*if(moveMouse.moveMouseDifferenceRotationY() != 0)
            {
                float diffrenceRotation = 0;
                if (moveMouse.moveMouseDifferenceRotationY() < -3)
                    diffrenceRotation = -3;
                else if (moveMouse.moveMouseDifferenceRotationY() > 3)
                    diffrenceRotation = 3;
                else
                    diffrenceRotation = moveMouse.moveMouseDifferenceRotationY();

                kickWeapon.transform.localRotation = Quaternion.Euler(kickWeapon.transform.localRotation.eulerAngles - new Vector3(diffrenceRotation, 0, 0));
                print("test");
            }*/

            kickWeapon.transform.localRotation = Quaternion.Euler(kickWeapon.transform.localRotation.eulerAngles - new Vector3(kickUP, Random.Range(-kickSideway, kickSideway), 0));
        }
    }
}


