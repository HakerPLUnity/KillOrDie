﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class LineRender : MonoBehaviour
    {
        public float delayRender = 0.5f;
        public Transform endLine;

        private float timeDelay;
        private LineRenderer line;

        // Use this for initialization
        void Start()
        {
            line = GetComponent<LineRenderer>();
            timeDelay = 0;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            timeDelay += Time.deltaTime;
            if (timeDelay >= delayRender)
            {
                line.SetPosition(0, transform.position);
                line.SetPosition(1, endLine.position);
            }
        }
    }
}