﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class Bullet : MonoBehaviour
    {
        public GameObject bulletHole;
        public float floatFrontWall = 0.02f;

        private Vector3 oldPosition;
        private float distance;

        private Vector3 moveDirection;
        private float speed;
        private float force;
        private int damage;
        private float gravity;
        private float liveTime;

        private int shotFromObject;

        private BodyPart bodyPart;
        private TestDmgBodyPart testDmg;

        private float timeToDestroy;
        private RaycastHit hit;

        // Use this for initialization
        void Awake()
        {
            timeToDestroy = 0;
            oldPosition = transform.position;
            distance = 0;

            liveTime = 5f;
        }
        
        void Update()
        {
            distance = Vector3.Distance(transform.position, oldPosition);
            if (Physics.Raycast(oldPosition, (transform.position - oldPosition), out hit, distance))
            {
                if (hit.transform.GetInstanceID() == shotFromObject)
                {
                    print(" hit.transform.GetInstanceID() = " + hit.transform.GetInstanceID() + "  shotFromObject = " + shotFromObject);
                    print("break InstanceID");
                    return;
                }

                if (hit.transform.tag.Equals("BodyPart"))
                {
                    hit.collider.SendMessage("Damage", damage);
                }
                //else if((testDmg = hit.transform.GetComponent<TestDmgBodyPart>()) != null)
                //{
                //    testDmg.changeText(damage);
                //}
                else
                {
                    float scaleHole = Random.Range(0.2f, 0.5f);
                    //tworzymy dziure po kuli oraz przypisujemy jej rodzica
                    GameObject bullet = (GameObject)Instantiate(bulletHole, hit.point + (hit.normal * (floatFrontWall + Random.Range(0, 0.01f))), Quaternion.LookRotation(hit.normal));
                    bullet.transform.localScale = new Vector3(scaleHole, scaleHole, 0);
                    bullet.transform.parent = hit.transform;

                }

                Destroy(gameObject);
            }

            oldPosition = transform.position;
        }

        void FixedUpdate()
        {
            //pocisk ma patrzec na swoja "nastepna" pozycje zeby bylo zludzenie ze leci po luku
            transform.rotation = Quaternion.LookRotation(moveDirection);
            transform.position += moveDirection * Time.deltaTime;
            moveDirection.y -= gravity * Time.deltaTime;

            //rotacja pocisku na osi z
            //transform.Rotate(new Vector3(0, 0, Random.Range(-180.0f, 180.0f)));

            timeToDestroy += Time.deltaTime;
            if(timeToDestroy >= liveTime)
            {
                Destroy(gameObject);
            }
        }


        public void updateData(Vector2 recoil, float _speed , float _force , int _damage , int _shotFromObject , float _gravity = 9.8f , float _liveTime = 20f)
        {
            //up
            transform.Rotate(Vector3.right, recoil.y);
            //sideway
            transform.Rotate(Vector3.up, recoil.x);
            //transform.Rotate(Vector3.forward, recoil.z, Space.World);

            speed = _speed;
            force = _force;
            damage = _damage;
            shotFromObject = _shotFromObject;
            gravity = _gravity;
            liveTime = _liveTime;

            moveDirection = transform.forward * speed;

            //Debug.Break();
        }
    }
}
