﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class TriggerWeaponScript : MonoBehaviour
    {
        public string Tag = AR.ConstVariable._playerTag;

        public bool isTriggerTrue()
        {
            return trigger;
        }

        public void setTriggerFalse()
        {
            trigger = false;
        }

        //private CapsuleCollider sphere;
        //private MeleeAtackEnemy atackMelee;
        private bool trigger;
        // Use this for initialization
        private void Start()
        {
            trigger = false;
            //sphere = GetComponent<CapsuleCollider>();
            //atackMelee = GetComponentInParent<MeleeAtackEnemy>();
        }

        /*
        private void FixedUpdate()
        {
            Collider[] col = Physics.OverlapSphere(transform.position, sphere.radius);
            foreach (Collider coll in col)
            {
                if (coll.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    trigger = true;
                    print("Enter coll.gameObject.tag " + coll.gameObject.tag);
                    return;
                }
            }

            trigger = false;
        }
        */


        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.transform.parent != null)
            {
                //jesli gracz wszedl w zasieg kolajdera
                if (other.gameObject.transform.parent.tag == Tag)
                {
                    trigger = true;
                }
            }
            else
            {
                //jesli gracz wszedl w zasieg kolajdera
                if (other.gameObject.tag == Tag)
                {
                    trigger = true;
                }
            }
        }



        /*
        private void OnTriggerEnter(Collider other)
        {

            Collider[] col = Physics.OverlapSphere(transform.position, sphere.radius);

            foreach (Collider coll in col)
            {
                if (coll.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    trigger = true;
                    print("Enter coll.gameObject.tag " + coll.gameObject.tag);
                    return;
                }
            }


            //jesli gracz wszedl w zasieg kolajdera
            /*if (other.gameObject.tag == AR.ConstVariable._playerTag)
            {
                trigger = true;
                print("Enter trigger " + trigger);
                print("Enter other.gameObject.tag " + other.gameObject.tag);
                print("Enter other.gameObject.name " + other.gameObject.name);
            }*/
        //}

        /*
        private void OnTriggerExit(Collider other)
        {

            Collider[] col = Physics.OverlapSphere(transform.position, sphere.radius);

            foreach (Collider coll in col)
            {
                if (coll.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    trigger = false;
                    print("Exit coll.gameObject.tag " + coll.gameObject.tag);
                    return;
                }
            }

            /*if (other.gameObject.transform.parent != null)
            {
                //jesli gracz wyszedl z zasiegu szukania
                if (other.gameObject.transform.parent.tag == AR.ConstVariable._playerTag)
                {
                    trigger = false;
                    print("Exit1 trigger " + trigger);
                    print("Exit1 other.gameObject.tag " + other.gameObject.tag);
                    print("Exit1 other.gameObject.name " + other.gameObject.name);
                }
            }
            else
            {
                //jesli gracz wyszedl z zasiegu szukania
                if (other.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    trigger = false;
                    print("Exit2 trigger " + trigger);
                    print("Exit2 other.gameObject.tag " + other.gameObject.tag);
                    print("Exit2 other.gameObject.name " + other.gameObject.name);
                }
            }

            //jesli gracz wyszedl z zasiegu szukania
            /*if (other.gameObject.tag == AR.ConstVariable._playerTag)
            {
                trigger = false;
                print("Exit trigger " + trigger);
                print("Exit other.gameObject.tag " + other.gameObject.tag);
                print("Exit other.gameObject.name " + other.gameObject.name);
            }*/

        /*

        //jesli gracz wyszedl z zasiegu szukania
        if (other.gameObject.tag == AR.ConstVariable._playerTag && trigger)
        {
            trigger = false;
            print("Exit trigger " + trigger);
            print("Exit other.gameObject.tag " + other.gameObject.tag);
            print("Exit other.gameObject.name " + other.gameObject.name);
        }
        */
        //}

    }
}
