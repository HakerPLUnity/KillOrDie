﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using AR;

namespace AR
{
    public class FPS : MonoBehaviour
    {
        public int fps = 30;
        public GameObject text;
        public float updateInterval = 0.5f;

        private Text gui;

        private float accum = 0.0f; // FPS accumulated over the interval
        private float frames = 0; // Frames drawn over the interval
        private float timeleft ; // Left time for current interval

        void Awake()
        {
            // 0 for no sync, 1 for panel refresh rate, 2 for 1/2 panel rate
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = fps;
        }

        void Start()
        {
            gui = text.GetComponent<Text>();
            //if (!GetComponent<GUIText>())
            //{
            //    print("FramesPerSecond needs a GUIText component!");
            //    enabled = false;
            //    return;
            //}
            timeleft = updateInterval;
        }

        // Update is called once per frame
        void Update()
        {
            timeleft -= Time.deltaTime;
            accum += Time.timeScale / Time.deltaTime;
            ++frames;

            // Interval ended - update GUI text and start new interval
            if (timeleft <= 0.0)
            {
                // display two fractional digits (f2 format)
                gui.text = "FPS - " + (accum / frames).ToString("f2");
                timeleft = updateInterval;
                accum = 0.0f;
                frames = 0;
            }
        }
    }
}
