﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public static class ConstVariable
    {
        //ignorowane maski dla raycast
        public static LayerMask _ignoreMaskRay = ~(1 << LayerMask.NameToLayer("ignoreRay"));
        //jaki procent wysokosci gracza dodac do "strzelenia" raycast przy sprawdzaniu
        //czy widzimy gracza (celowanie w glowe)
        public static int _percentageOfPlayerHeight = 30;

        public static string _playerTag = "Player";

        public static string _enemyTag = "Enemy";
        public static string _atackColliderTag = "AtackCollider";
    }
}
