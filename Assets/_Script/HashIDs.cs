﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class HashIDs : MonoBehaviour
    {

        //public int guardTrigger;
        //public int attack1Trigger;
        //public int attack2Trigger;
        public int speedFloat;
        public int moveState;
        public int guardState;
        public int attack1State;
        public int attack2State;

        public int dead;

        // Use this for initialization
        void Awake()
        {
            //guardTrigger = Animator.StringToHash("Guard");
            //attack1Trigger = Animator.StringToHash("Attack1");
            //attack2Trigger = Animator.StringToHash("Attack2");
            speedFloat = Animator.StringToHash("Speed");
            moveState = Animator.StringToHash("Troll Layer.Move");
            guardState = Animator.StringToHash("Troll Layer.Guard");
            attack1State = Animator.StringToHash("Troll Layer.Attack_01");
            attack2State = Animator.StringToHash("Troll Layer.Attack_02");
            dead = Animator.StringToHash("Dead");
        }
    }
}
