﻿using UnityEngine;
using System.Collections;

public class HideCoursor : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        //blokujemy kursor na srodku ekranu
        Cursor.lockState = CursorLockMode.Locked;
        //chowamy kursor
        Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
