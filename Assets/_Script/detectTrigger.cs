﻿using UnityEngine;
using System.Collections;

public class detectTrigger : MonoBehaviour {

    float distance;

    Vector3 oldPosition;

    void Start()
    {
        distance = 0;
        oldPosition = transform.position;
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        distance = Vector3.Distance(transform.position, oldPosition);
        if (Physics.Raycast(oldPosition, (transform.position - oldPosition), out hit, distance))
        {
            printName(hit.transform.name + " Update");
        }

        oldPosition = transform.position;
    }

    void OnTriggerEnter(Collider other)
    {
        printName(other.gameObject.name + " OnTriggerEnter");

    }

    void printName(string name)
    {
        print(name);
    }
}
