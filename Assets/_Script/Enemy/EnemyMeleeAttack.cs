﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    abstract public class EnemyMeleeAttack : EnemyBaseParam
    {
        //CZAS MIEDZY ATAKAMI
        public float timeBetweenAtack = 4f;
        //OBRAZENIA JAKIE ZADAJE GRACZOWI
        public int Damage = 5;

        protected bool attack;

        //REFERENCJA NA SKRYPT GRACZA
        protected PlayerHealth playerHealth;
        //REFERENCJA NA SKRYPT PRZECIWNIKA
        protected EnemyHealth enemyHealth;

        protected TriggerColliderScript playerInRange;
        protected TriggerWeaponScript atackCollider;

        //OBECNY CZAS (POTRZEBNY DO SYNCHRONIZACJI ATAKU)
        protected float timerAtack;

        abstract public bool isPlayerInRange();

        abstract public bool MeleeAttack();

        abstract public void resetTrigger();

        abstract public void Attack(string attackMethod = "default");

        protected override void Start()
        {
            base.Start();

            attack = false;
            //PRZYPISUJEMY KOMPONENTY I OBIEKTY DO ZMIENNYCH
            this.enemyHealth = GetComponent<EnemyHealth>();
            this.playerHealth = player.GetComponent<PlayerHealth>();
        }

        protected virtual void Update()
        {
            timerAtack += Time.deltaTime;
        }
    }
}
