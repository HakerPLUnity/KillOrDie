﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class TrollMeleeAttack : EnemyMeleeAttack
    {
        public override bool isPlayerInRange()
        {
            if(playerHealth.CurrentPlayerHealth() <= 0)
            {
                return false;
            }
            //print("atackCollider.isInCollider() " + playerInRange.isInCollider());
            return playerInRange.isInCollider();
        }

        public override bool MeleeAttack()
        {
            if (this.playerInRange.isInCollider())
            {
                if (timerAtack >= timeBetweenAtack && !this.animatorEnemy.GetCurrentAnimatorStateInfo(0).IsName("Troll Layer.Attack_01"))
                {
                    timerAtack = 0;
                    this.animatorEnemy.Play(this.hash.attack1State);

                    //print("atack ");
                    return true;
                }
            }

            if (this.animatorEnemy.GetCurrentAnimatorStateInfo(0).IsName("Troll Layer.Attack_01"))
            {
                return true;
            }
            return false;
        }

        public override void resetTrigger()
        {
            attack = false;
            atackCollider.setTriggerFalse();
        }

        public override void Attack(string attackMethod = "default")
        {
            if (attack)
            {
                return;
            }


            switch (attackMethod)
            {
                case "normal":
                    //print("normal attack");
                    break;

                case "strong":
                    //print("strong attack");
                    break;

                default:
                    //print("default attack");
                    break;
            }

            if (atackCollider.isTriggerTrue())
            {
                attack = true;
                atackCollider.setTriggerFalse();
                //print("hit player");
                playerHealth.TakeDamage(Damage);
            }
        }

        protected override void Start()
        {
            base.Start();
            this.atackCollider = gameObject.GetComponentInChildren<TriggerWeaponScript>();
            this.playerInRange = gameObject.FindGameObjectInChildByName<TriggerColliderScript>("Range_Atack");

            if (this.playerInRange == null)
            {
                print("MeleeAtackEnemy this.turnToPlayer is null");
            }

            if (this.atackCollider == null)
            {
                print("MeleeAtackEnemy atackCollider is null");
            }
        }
    }
}
