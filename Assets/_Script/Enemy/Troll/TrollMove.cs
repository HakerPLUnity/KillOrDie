﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class TrollMove : EnemyMove
    {
        protected float speedDampTime = 0.1f;

        float speed;

        override public void setDestinationCurrentPosition(Vector3 destinyPosition)
        {
            this.meshAgent.velocity = Vector3.zero;
            this.meshAgent.SetDestination(destinyPosition);
        }

        override public bool isAnimationPlay(string name)
        {
            if(this.animatorEnemy.GetCurrentAnimatorStateInfo(0).IsName(name))
            {
                //print(name + " state");
                return true;
            }
            return false;
        }

        override public void moveEnemyToPlayer(Vector3 playerPosition)
        {
            this.meshAgent.speed = changeValueOfField(this.meshAgent.speed, this.normalMoveSpeed, this.runMoveSpeed);
            moveToPlayerIfHeFar(playerPosition);
        }

        override public bool moveEnemyPatrol()
        {
            this.meshAgent.speed = changeValueOfField(this.meshAgent.speed, this.runMoveSpeed, this.normalMoveSpeed);
            return pointSearch(false);
        }

        override public bool moveEnemyInvestigation(Vector3 positionInvestigation)
        {
            this.meshAgent.speed = changeValueOfField(this.meshAgent.speed, this.normalMoveSpeed, this.runMoveSpeed);

            return pointSearch(true, positionInvestigation);
        }

        override public void playGuard()
        {
            this.animatorEnemy.Play(this.hash.guardState);
        }

        override public void playMove()
        {
            this.animatorEnemy.Play(this.hash.moveState);
        }
        
        void Update()
        {
            speed = Vector3.Project(this.meshAgent.desiredVelocity, transform.forward).magnitude;
            this.animatorEnemy.SetFloat(hash.speedFloat, speed, speedDampTime, Time.deltaTime);
        }

        new protected void Start()
        {
            base.Start();
        }

        private void moveToPlayerIfHeFar(Vector3 playerPosition)
        {
            this.meshAgent.SetDestination(playerPosition);

            if (this.meshAgent.remainingDistance <= this.meshAgent.stoppingDistance)
            {
                this.actual_time_to_start_go_again_to_player = 0;
                rotateEnemyToPlayer(playerPosition);
                //zerujemy "addforce"distanceToPlayerClose
                this.meshAgent.velocity = Vector3.zero;
                this.meshAgent.Stop();
            }

            if(Vector3.Distance(this.transform.position, playerPosition) > this.distanceToPlayer)
            {
                this.actual_time_to_start_go_again_to_player += Time.deltaTime;
            }

            if(this.time_to_start_go_again_to_player <= this.actual_time_to_start_go_again_to_player)
            {
                resumeNavMesh();
            }
        }

        private void rotateEnemyToPlayer(Vector3 playerPosition)
        {
            Vector3 targetDir = playerPosition - this.transform.position;
            float step = 10 * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(this.transform.forward, targetDir, step, 0.0F);
            newDir.y = 0f;

            if (Quaternion.Angle(this.transform.rotation, Quaternion.LookRotation(newDir)) != 0)
            {
                this.transform.rotation = Quaternion.LookRotation(newDir);
            }
        }

        private bool pointSearch(bool investigation, Vector3 positionSearch = default(Vector3), Vector3 rangeSearch = default(Vector3))
        {
            //jesli nie mamy punktu do patlowania to go ustawiamy
            if (!this.havePathPatrol)
            {
                RandomPoint(out this.pointPatrol, investigation, positionSearch, rangeSearch);
                this.meshAgent.SetDestination(this.pointPatrol);
                this.havePathPatrol = true;
                //this.meshAgent.Resume();
                
                return false;
            }
            //jesli trasa jest stworzona ale nie gotowa??
            if (!this.meshAgent.pathPending)
            {
                if (this.meshAgent.remainingDistance <= this.meshAgent.stoppingDistance)
                {
                    if (!this.meshAgent.hasPath || this.meshAgent.velocity.sqrMagnitude == 0f)
                    {
                        this.havePathPatrol = false;
                        return true;
                    }
                }
            }
            return false;
        }

        private float changeValueOfField(float field, float equalTo, float newValue)
        {
            if (field == equalTo)
            {
                return newValue;
            }
            return field;
        }
    }
}
