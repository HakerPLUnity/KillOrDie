﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class TrollBehavior : EnemyBehavior
    {
        // Use this for initialization
        new protected void Start()
        {
            base.Start();
        }

        override protected void Patrol()
        {
            if (this.enemySight.getPlayerInSight() == true)
            {
                print("patrol sight");
                this.timeToNewPointPatrol = 0;
                this.status = enemyStateAllowed.Chase;
                return;
            }

            if (this.enemyMeleeAttack.isPlayerInRange())
            {
                print("patrol");
                this.timeToNewPointPatrol = 0;
                this.status = enemyStateAllowed.Atack;
                return;
            }

            if (this.enemyMove.moveEnemyPatrol())
            {
                this.timeToNewPointPatrol = 0;
                this.status = enemyStateAllowed.Guard;
                return;
            }

            this.newPositionRound.x = Mathf.Round(this.transform.position.x);
            this.newPositionRound.y = Mathf.Round(this.transform.position.y);
            this.newPositionRound.z = Mathf.Round(this.transform.position.z);

            this.timeToNewPointPatrol += Time.deltaTime;
            if (this.timeToNewPointPatrol >= this.timeToFindNewPointPatrol)
            {
                this.timeToNewPointPatrol = 0;
                if (this.oldPositionRound == this.newPositionRound)
                {
                    this.enemyMove.setDestinationCurrentPosition(this.transform.position);
                    this.enemyMove.moveEnemyPatrol();
                    return;
                }
                this.oldPositionRound = this.newPositionRound;
            }
        }

        override protected void Guard()
        {
            this.timeGuard += Time.deltaTime;

            if (this.enemySight.getPlayerInSight() == true)
            {
                this.guardRun = false;
                this.timeGuard = 0;
                this.randomTimeGuard = Random.Range(0, this.longGuard - 1);
                this.status = enemyStateAllowed.Chase;
                this.enemyMove.playMove();
                return;
            }

            if (this.enemyMeleeAttack.isPlayerInRange())
            {
                this.guardRun = false;
                this.timeGuard = 0;
                this.randomTimeGuard = Random.Range(0, this.longGuard - 1);
                this.status = enemyStateAllowed.Atack;
                return;
            }

            if (!this.guardRun)
            {
                if (this.randomTimeGuard > this.longGuard * 0.3f && this.timeGuard >= this.randomTimeGuard)
                {
                    this.guardRun = true;
                    this.enemyMove.playGuard();
                }
            }

            if (this.timeGuard >= this.longGuard && !this.enemyMove.isAnimationPlay("Troll Layer.Guard"))
            {
                this.guardRun = false;
                this.timeGuard = 0;
                this.randomTimeGuard = Random.Range(0, this.longGuard - 1);
                this.status = enemyStateAllowed.Patrol;
                return;
            }
        }

        override protected void Chase()
        {
            this.timeInvestigate += Time.deltaTime;

            //jesli zgubilismy gracza to jeszcze przez chwile podazamy za nim (do czasu rozpoczecia poszukiwania)
            if (this.enemySight.getPlayerInSight() == false)
            {
                this.enemyMove.moveEnemyToPlayer(this.player.transform.position);
            }
            //jesli widzimy gracza to kierujemy sie na jego pozycje
            if (this.enemySight.getPlayerInSight() == true)
            {
                this.enemyMove.moveEnemyToPlayer(this.enemySight.getPositionOfPlayer());
            }


            if (this.enemySight.getPlayerInSight() == false && this.timeInvestigate >= this.timeToInvestigate)
            {
                this.timeInvestigate = 0;
                this.havePlaceToInvestigation = false;
                this.enemySight.addPosition(this.player.transform.position);
                this.status = enemyStateAllowed.Investigate;
                return;
            }
            

            if (this.enemySight.getPlayerInSight() == null)
            {
                this.enemyMove.resumeNavMesh();
                this.enemySight.resetPositon();
                this.status = enemyStateAllowed.Guard;
                return;
            }

            

            if (this.enemyMeleeAttack.isPlayerInRange())
            {
                this.timeInvestigate = 0;
                this.status = enemyStateAllowed.Atack;
                return;
            }
        }

        override protected void Atack()
        {
            if(this.enemyMeleeAttack.MeleeAttack())
            {
                return;
            }
            this.status = enemyStateAllowed.Chase;
        }

        override protected void Investigate()
        {
            if (this.enemySight.getPlayerInSight() == true)
            {
                this.currentInvestigatePoint = 0;
                this.havePlaceToInvestigation = false;
                this.status = enemyStateAllowed.Chase;
                this.enemyMove.playMove();
                return;
            }

            if (this.currentInvestigatePoint >= this.muchSearchPlayerInInvestigate)
            {
                this.havePlaceToInvestigation = false;
                this.enemySight.resetPositon();
                this.currentInvestigatePoint = 0;
                this.status = enemyStateAllowed.Guard;
                return;
            }

            if (!this.havePlaceToInvestigation)
            {
                this.havePlaceToInvestigation = true;
                this.searchPlace = this.enemySight.getWalkDirectionPlayer();
            }

            if (!this.enemyMove.isAnimationPlay("Troll Layer.Guard") && this.enemyMove.moveEnemyInvestigation(this.searchPlace))
            {
                this.currentInvestigatePoint++;
                this.enemyMove.playGuard();
            }

            if (this.enemyMeleeAttack.isPlayerInRange())
            {
                this.currentInvestigatePoint = 0;
                this.havePlaceToInvestigation = false;
                this.status = enemyStateAllowed.Atack;
                return;
            }
        }
    }
}
