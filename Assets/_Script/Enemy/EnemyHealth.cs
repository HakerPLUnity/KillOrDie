﻿using UnityEngine;
using UnityEngine.UI ;
using System.Collections;
using AR;

namespace AR
{
    public class EnemyHealth : EnemyBaseParam
    {
        //ILE ZYCIA BEDZIE MIAL ENEMY
        public int enemyHealth = 100;
        //SZYBKOSC ZNIKNIECIA POD PODLOGE
        public float fastGoDown = 0.1f;
        public float destroyTime = 2f;

        private float time;

        //OBECNE ZYCIE GRACZA
        private int currentHealthEnemy;

        //ZMIENNE BOOL DO SMIERCI LUB DAMAGE GRACZA
        private bool EnemyDead;
        private bool isGoDown;

        new protected void Start()
        {
            base.Start();
            time = 0;
            //USTAWIAMY POCZATKOWE ZYCIE GRACZA
            currentHealthEnemy = enemyHealth;
        }


        // Update is called once per frame
        private void Update()
        {
            //JESLI ENEMY NIE ZYJE I MA ZNIKNAC POD PODLOGA
            if (isGoDown)
            {
                //PRZESUWAMY PRZECIWNIKIEM POD PODLOGE
                transform.Translate(-Vector3.up * fastGoDown * Time.deltaTime);
            }

        }


        //FUNKCJA UZYWANA PRZEZ INNE OBIEKTY ZEBY ZADAC OBRARZENIA GRACZOWI
        public void TakeDamage(int HowMuch)
        {
            //JESLI PRZECIWNIK NIE ZYJE TO NIE MA CO SPRAWDZAC CO Z NIM
            if (EnemyDead)
                return;

            //ODEJMUJEMY OD ZYCIA OBRAZENIA
            currentHealthEnemy -= HowMuch;

            //print("damage = " + HowMuch + "  health = " + currentHealthEnemy);

            //SPRAWDZAMY CZY ENEMY ZYJE 
            if (currentHealthEnemy <= 0)
            {
                currentHealthEnemy = 0;
                HeIsDeath();
            }
        }


        public int CurrentEnemyHealth()
        {
            return currentHealthEnemy;
        }


        //FUNKCJA UZYWANA PRZEZ EVENT ZARAZ PO ANIMACJI "Dead" (TRZEBA TAM USTAWIC ZEBY SIE WLACZYLA)
        public void StartGoDown()
        {
            //ENEMY IDZIE W DOL
            isGoDown = true;

            //WYLACZAMY NavMesh ZEBY MARTWY PRZECIWNIK NIE STARAL SIE PORUSZAC
            GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
            GetComponent<EnemyBehavior>().enabled = false;
            GetComponent<EnemyMeleeAttack>().enabled = false;
            GetComponent<EnemyMove>().enabled = false;

            animatorEnemy.SetTrigger(hash.dead);

            //NISZCZYMY OBIEKT PRZECIWNIKA PO OKRESLONYM CZASIE
            Destroy(gameObject, destroyTime);
        }


        private void HeIsDeath()
        {
            //ENEMY NIE ZYJE
            EnemyDead = true;

            StartGoDown();
        }
    }
}
