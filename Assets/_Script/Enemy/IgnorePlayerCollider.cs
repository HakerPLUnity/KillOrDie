﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class IgnorePlayerCollider : MonoBehaviour
    {
        // Use this for initialization
        void Start()
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag(AR.ConstVariable._playerTag);
            foreach(GameObject player in players)
            {
                Physics.IgnoreCollision(GetComponent<Collider>(), player.GetComponent<Collider>());
            }
        }
    }
}
