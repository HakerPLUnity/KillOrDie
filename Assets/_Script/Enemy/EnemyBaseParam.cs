﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    abstract public class EnemyBaseParam : MonoBehaviour
    {
        //animacja przeciwnika
        protected Animator animatorEnemy;
        protected HashIDs hash;
        protected GameObject player;

        protected virtual void Start()
        {
            this.player = GameObject.FindGameObjectWithTag(AR.ConstVariable._playerTag);
            this.hash = GetComponent<HashIDs>();
            this.animatorEnemy = GetComponent<Animator>();
        }
    }
}
