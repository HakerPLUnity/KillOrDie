﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AR;

namespace AR
{
    [System.Serializable]
    public class sightData
    {
        public float angle;                 //zasieg
        public float distance;              //dystans
        public float secondsVisibility;     //czas potrzebny na zobaczenie przeciwnika w danej strefie
        public Color color;                 //kolor raycasta
    }

    public class EnemySight : MonoBehaviour
    {
        //dane do strefy widzenia
        public List<sightData> dataSight;
        //czy radius w SphereCollider ma miec wielosc najwiekszego zasiegu
        public bool sightDistanceEqualSphereRadius = false;
        //czy rysowac gizmo
        public bool drawGizmo = true;
        //co ile ma zapisywac pozycje gracza
        public float timeToSavePositionPlayer = 0.5f;

        private float timeSavePosition;

        //zachowanie przeciwnika (czy szuka gracza czy tylko patroluje)
        private TrollBehavior behavior;
        //gracz w zasiegu kolajdera
        private bool playerInSight;
        private UnityEngine.AI.NavMeshAgent nav;

        //pozycja w ktorej widzimy gracza
        private Vector3 sightPlayerPosition;

        //zmienna pomocnicza przy pobraniu SphereCollider w gizmo
        private bool colGet = false;

        private float timeVisible = 0;

        //Colider dla slysznia przeciwnika OnTriggerExit nie dziala poprawnie
        private SphereCollider col;
        //obiekt gracza
        private GameObject player;
        //zycie gracza
        private PlayerHealth playerHealth;
        //jakie maski ma ignorowac Raycast
        private int ignoreRay;

        private Vector3[] positionPlayer;

        //odchylenie dla raycast
        private Vector3 directionAngleRayLeft;
        private Vector3 directionAngleRayRight;

        public bool? getPlayerInSight()
        {
            if (playerHealth.CurrentPlayerHealth() <= 0)
            {
                return null;
            }

            return playerInSight;
        }

        public Vector3 getPositionOfPlayer()
        {
            return sightPlayerPosition;
        }

        public bool havePositionPlayerInvestigation()
        {
            return positionPlayer[0] != Vector3.zero;
        }

        public Vector3 getWalkDirectionPlayer()
        {
            return positionPlayer[0] + (positionPlayer[0] - positionPlayer[1]);
        }

        public void resetPositon()
        {
            for(int i = 0; i < positionPlayer.Length; i++)
            {
                positionPlayer[i] = Vector3.zero;
            }
        }

        public void addPosition(Vector3 position)
        {
            for (int i = positionPlayer.Length - 1; i > 0; i--)
            {
                positionPlayer[i] = positionPlayer[i - 1];
            }
            positionPlayer[0] = position;
        }

        private void Awake()
        {
            //pobieramy potrzebne komponenty
            nav = GetComponentInParent<UnityEngine.AI.NavMeshAgent>();
            col = GetComponent<SphereCollider>();
            behavior = GetComponentInParent<TrollBehavior>();
            player = GameObject.FindGameObjectWithTag("Player");
            playerHealth = player.GetComponent<PlayerHealth>();

            //jesli wielkosc kolajdera ma byc rowna najwiekszemu zasiegowi
            //to szukamy najwiekszego i przypisujem jego wartosc
            if (sightDistanceEqualSphereRadius)
            {
                float distance = 0;
                foreach (sightData sight in dataSight)
                {
                    if(distance < sight.distance)
                    {
                        distance = sight.distance;
                    }
                }
                col.radius = distance;
            }
            //ustawiamy wartosc poczatkowa
            playerInSight = false;
        }

        private void Start()
        {
            //przy rozpoczeciu gry nie rysujemy gizmo
            drawGizmo = false;
            //ustawiamy jaka maske ma ignorowac raycast
            ignoreRay = ConstVariable._ignoreMaskRay;

            this.positionPlayer = new Vector3[2];
            //resetujemy pozycje gracza
            this.resetPositon();
        }

        private void FixedUpdate()
        {
            //rysujemy zasieg widzenia w trakcie gry
            if (drawGizmo)
            {
                for (int i = 0; i < dataSight.Count; i++)
                {
                    directionAngleRayRight = Quaternion.AngleAxis(dataSight[i].angle, transform.up) * transform.forward;
                    directionAngleRayLeft = Quaternion.AngleAxis(-dataSight[i].angle, transform.up) * transform.forward;

                    Debug.DrawRay(transform.position, transform.forward * dataSight[i].distance, dataSight[i].color, dataSight[i].distance, true);
                    Debug.DrawRay(transform.position, directionAngleRayRight * dataSight[i].distance, dataSight[i].color, dataSight[i].distance, true);
                    Debug.DrawRay(transform.position, directionAngleRayLeft * dataSight[i].distance, dataSight[i].color, dataSight[i].distance, true);
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.transform.parent != null)
            {
                //jesli gracz wszedl w zasieg kolajdera
                if (other.gameObject.transform.parent.tag == AR.ConstVariable._playerTag)
                {
                    //ustawiamy poczatkowo ze nie widzimy gracza
                    playerInSight = false;
                    //jesli gracz nie zyje to nie interesuje nas czy go widzimy czy nie
                    if (playerHealth.CurrentPlayerHealth() <= 0)
                    {
                        return;
                    }

                    //sprawdzamy czy widzimy gracza
                    SightPlayer(other.gameObject.transform.parent.position, player.GetComponent<CharacterController>().center.y, player.GetComponent<CharacterController>().height);
                }
            }
            else
            {
                //jesli gracz wszedl w zasieg kolajdera
                if (other.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    //jesli gracz nie zyje to nie interesuje nas czy go widzimy czy nie
                    if (playerHealth.CurrentPlayerHealth() <= 0)
                    {
                        return;
                    }
                    //ustawiamy poczatkowo ze nie widzimy gracza
                    playerInSight = false;

                    //sprawdzamy czy widzimy gracza
                    SightPlayer(other.transform.position, player.GetComponent<CharacterController>().center.y, player.GetComponent<CharacterController>().height);
                }
            }
        }

        private void SightPlayer(Vector3 positionCollider, float centerColliderY, float heightCollider)
        {
            //print("testttttt");
            //tworzymy kierunek dla strzalu w glowe i w klate
            Vector3 chest = positionCollider - transform.position;

            positionCollider.y += centerColliderY + ((heightCollider * ConstVariable._percentageOfPlayerHeight) / 100);
            Vector3 head = positionCollider - transform.position;

            //pobieramy kont miedzy kierunkiem a przodem przeciwnika
            float angle1 = Vector3.Angle(head, transform.forward);
            float angle2 = Vector3.Angle(chest, transform.forward);

            //przelatujemy po wszystkich strafach widzenia 
            foreach (sightData sight in dataSight)
            {
                //jesli kont jest mniejszy od kontu danego zasiegu oraz przeciwnik jest sciga lub szuka gracza
                //to strzelamy w glowe
                if (angle1 < sight.angle && (behavior.getState() == enemyStateAllowed.Chase ||
                    behavior.getState() == enemyStateAllowed.Investigate))
                {
                    if (CheckRaycast(head, sight, "head"))
                    {
                        //print("testttttt11111");
                        return;
                    }
                }
                //strzelamy w klate
                if (angle2 < sight.angle)
                {
                    if (CheckRaycast(chest, sight, "chest"))
                    {
                        //print("testttttt22222");
                        return;
                    }
                }

                // Store the name hashes of the current states.
                /*int playerLayerZeroStateHash = playerAnim.GetCurrentAnimatorStateInfo(0).nameHash;
                int playerLayerOneStateHash = playerAnim.GetCurrentAnimatorStateInfo(1).nameHash;

                / If the player is running or is attracting attention...
                if (playerLayerZeroStateHash == hash.locomotionState || playerLayerOneStateHash == hash.shoutState)
                {
                    // ... and if the player is within hearing range...
                    if (CalculatePathLength(player.transform.position) <= col.radius)
                        // ... set the last personal sighting of the player to the player's current position.
                        personalLastSighting = player.transform.position;
                }*/
            }
        }

        //nie diala poprawnie przy wiekszej ilosci koliderow na obiekcie
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.transform.parent != null)
            {
                //jesli gracz wyszedl z zasiegu szukania
                if (other.gameObject.transform.parent.tag == AR.ConstVariable._playerTag)
                {
                    playerInSight = false;
                }
            }
            else
            {
                //jesli gracz wyszedl z zasiegu szukania
                if (other.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    playerInSight = false;
                }
            }
        }


        private float CalculatePathLength(Vector3 targetPosition)
        {
            // Create a path and set it based on a target position.
            UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
            if (nav.enabled)
                nav.CalculatePath(targetPosition, path);

            // Create an array of points which is the length of the number of corners in the path + 2.
            Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];

            // The first point is the enemy's position.
            allWayPoints[0] = transform.position;

            // The last point is the target position.
            allWayPoints[allWayPoints.Length - 1] = targetPosition;

            // The points inbetween are the corners of the path.
            for (int i = 0; i < path.corners.Length; i++)
            {
                allWayPoints[i + 1] = path.corners[i];
            }

            // Create a float to store the path length that is by default 0.
            float pathLength = 0;

            // Increment the path length by an amount equal to the distance between each waypoint and the next.
            for (int i = 0; i < allWayPoints.Length - 1; i++)
            {
                pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
            }

            return pathLength;
        }


        private bool CheckRaycast(Vector3 direction, sightData sight, string test)
        {
            RaycastHit hit;

            //sprawdzamy czy raycast trafil w cos
            if (Physics.Raycast(transform.position, direction.normalized, out hit, sight.distance, ignoreRay))
            {
                //print("raycast");
                //sprawdzay czy to gracz
                if (hit.collider.gameObject.tag == AR.ConstVariable._playerTag)
                {
                    //print("raycast player");
                    sightPlayerPosition = hit.collider.gameObject.transform.position;
                    //czas potrzebny przy zapisywaniu pozycji gracza
                    timeSavePosition += Time.deltaTime;
                    //jesli szukamy gracza lub scigamy to odrazu go widzimy
                    if (behavior.getState() == enemyStateAllowed.Chase ||
                       behavior.getState() == enemyStateAllowed.Investigate)
                    {
                        timeVisible = 0;

                        //actualSectorView = null;
                        if (timeSavePosition > timeToSavePositionPlayer)
                        {
                            timeSavePosition = 0;
                            this.addPosition(sightPlayerPosition);
                        }

                        playerInSight = true;
                        return true;
                    }

                    //aktualizujemy czas i sprawdzamy czy uplynela wystarczajaca ilosc sekund zeby widziec gracza
                    timeVisible += Time.deltaTime;
                    if (timeVisible >= sight.secondsVisibility)
                    {
                        timeVisible = 0;

                        if (timeSavePosition > timeToSavePositionPlayer)
                        {
                            timeSavePosition = 0;
                            this.addPosition(sightPlayerPosition);
                        }

                        playerInSight = true;
                        return true;
                    }

                    return true;
                }
            }
            //zwracamy false jesli nie trafilismy w gracza
            return false;
        }

        private void OnDrawGizmos()
        {
            if(!drawGizmo)
            {
                return;
            }
            float distance = 0;

            //rysujemy strefy widzenia
            foreach (sightData sight in dataSight)
            {
                if (distance < sight.distance)
                {
                    distance = sight.distance;
                }
                directionAngleRayRight = Quaternion.AngleAxis(sight.angle, transform.up) * transform.forward;
                directionAngleRayLeft = Quaternion.AngleAxis(-sight.angle, transform.up) * transform.forward;

                Debug.DrawRay(transform.position, transform.forward * sight.distance, sight.color, sight.distance, true);
                Debug.DrawRay(transform.position, directionAngleRayRight * sight.distance, sight.color, sight.distance, true);
                Debug.DrawRay(transform.position, directionAngleRayLeft * sight.distance, sight.color, sight.distance, true);
            }

            //jesli zasieg kolajdera ma byc wielkosci najwiekszego zasiegu
            if (sightDistanceEqualSphereRadius)
            {
                //jesli jeszcze nie pobralismy kolajdera
                if (!colGet)
                {
                    colGet = true;
                    col = GetComponent<SphereCollider>();
                }
                col.radius = distance;
            }
        }
    }
}
