﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    abstract public class EnemyMove : EnemyBaseParam
    {
        //szubkosc poruszania sie podczas widzenia gracza
        public float runMoveSpeed = 0;
        //szubkosc skrecania podczas widzenia gracza
        public float runAngularSpeed = 0;
        //obszar patrolu
        public Vector3 rangePatrol = new Vector3(5, 5, 5);

        //podstawowa wielkosc poszukiwan
        public Vector3 investigationRange = new Vector3(5, 5, 5);
        //czy rysowac obszar patrolu
        public bool drawPatrolRange;

        public float time_to_start_go_again_to_player = 0.2f;
        protected float actual_time_to_start_go_again_to_player;

        protected UnityEngine.AI.NavMeshAgent meshAgent;
        protected EnemyBehavior enemyBehavior;
        protected float stopDistanceToPlayer;
        //potrzebna zmienna do pamietania poczatku patrolu 
        protected Vector3 startPosition;
        protected Vector3 pointPatrol;
        //informuje o tym czy enemy posiada punkt do patrolu
        //jesli nie to go szuka
        protected bool havePathPatrol;
        //potrzebna do okreslenia srodka enemy
        //protected float heightEnemy;
        //ustala srodek enemy
        //protected Vector3 positionRay;
        protected bool drawGizmoGame = false;

        protected float normalMoveSpeed;
        protected float normalAngularSpeed;

        //odleglosc w jakiej ma sie zatrzymac przeciwnik
        protected float distanceToPlayer;
        //jesli juz stoimy przy graczu to odleglosc od jakiej zaczynamy scigac gracza
        //musi byc wieksza niz wczesniejsza odleglosc
        //(zeby przeciwnik nie mial "drgawek"
        protected float distanceToPlayerClose;

        abstract public void setDestinationCurrentPosition(Vector3 destinyPosition);

        abstract public bool isAnimationPlay(string name);

        abstract public void moveEnemyToPlayer(Vector3 playerPosition);

        abstract public bool moveEnemyPatrol();

        abstract public bool moveEnemyInvestigation(Vector3 positionInvestigation);

        abstract public void playGuard();

        abstract public void playMove();

        public void resumeNavMesh()
        {
            this.meshAgent.Resume();
        }

        //zwraca losowy punkt do patrolowania
        protected bool RandomPoint(out Vector3 result, bool investigation, Vector3 positionSearch = default(Vector3), Vector3 rangeSearch = default(Vector3))
        {
            if (positionSearch == Vector3.zero)
            {
                positionSearch = this.startPosition;
            }
            if (rangeSearch == Vector3.zero)
            {
                if (investigation)
                {
                    rangeSearch = this.investigationRange;
                }
                else
                {
                    rangeSearch = rangePatrol;
                }
            }

            for (int i = 0; i < 100; i++)
            {
                Vector3 randomPoint = new Vector3(positionSearch.x + Random.insideUnitSphere.x * rangeSearch.x, positionSearch.y + Random.insideUnitSphere.y * rangeSearch.y, positionSearch.z + Random.insideUnitSphere.z * rangeSearch.z);
                UnityEngine.AI.NavMeshHit hit;
                //sprawdzamy czy punkt znajduje sie na navmesh i moze po nim chodzic przeciwnik (meshAgent.areaMask?)
                if (UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out hit, 2f, meshAgent.areaMask))
                {
                    //print("simpleposition true");
                    result = hit.position;
                    return true;
                }
            }
            print("simpleposition false");
            result = this.transform.position;
            return false;
        }

        protected void OnDrawGizmos()
        {
            //mnozymy zasieg przez 2 poniewaz wielkosc kwadrata jest obliczana jako
            //bok ma tyle co zasieg a my chcemy zeby od punktu centralnego ma byc wielkosc zasiegu = rangePatrol (czyli * 2)
            if (this.drawPatrolRange)
            {
                if (!this.drawGizmoGame)
                {
                    Gizmos.DrawWireCube(this.transform.position, new Vector3(this.rangePatrol.x * 2, this.rangePatrol.y * 2, this.rangePatrol.z * 2));
                }
                else
                {
                    Gizmos.DrawWireCube(this.startPosition, new Vector3(this.rangePatrol.x * 2, this.rangePatrol.y * 2, this.rangePatrol.z * 2));
                }
            }
        }

        new protected void Start()
        {
            base.Start();

            this.havePathPatrol = false;

            this.actual_time_to_start_go_again_to_player = this.time_to_start_go_again_to_player + 1f;

            this.enemyBehavior = GetComponent<EnemyBehavior>();
            //this.heightEnemy = GetComponent<CapsuleCollider>().height;
            //this.positionRay = new Vector3(0, this.heightEnemy / 2, 0);

            this.startPosition = this.transform.position;

            this.meshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            this.drawGizmoGame = true;

            if (this.runMoveSpeed == 0)
            {
                this.runMoveSpeed = this.meshAgent.speed * 2;
            }
            if (this.runAngularSpeed == 0)
            {
                this.runAngularSpeed = this.meshAgent.angularSpeed * 1.5f;
            }
            this.normalMoveSpeed = this.meshAgent.speed;
            this.normalAngularSpeed = this.meshAgent.angularSpeed;

            this.distanceToPlayer = this.stopDistanceToPlayer = meshAgent.stoppingDistance;
        }
    }
}
