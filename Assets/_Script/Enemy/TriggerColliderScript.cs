﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class TriggerColliderScript : MonoBehaviour
    {
        public string Tag = AR.ConstVariable._playerTag;

        public bool isInCollider()
        {
            return inCollider;
        }


        private bool inCollider;
        private bool helperBool;

        // Use this for initialization
        private void Start()
        {
            inCollider = false;
            helperBool = false;
        }

        private void OnTriggerStay(Collider other)
        {
            if(other.gameObject.tag == Tag)
            {
                helperBool = true;
            }
        }

        private void FixedUpdate()
        {
            if (helperBool)
            {
                inCollider = true;
                helperBool = false;
            }
            else
                inCollider = false;

        }
    }
}
