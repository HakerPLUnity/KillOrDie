﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    public class BodyPart : MonoBehaviour
    {
        //ile ma zadaz borazen z broni (1f = 100%)
        public float multiplierDamage  = 1f;

        private EnemyHealth enemyHealth;

        void Start()
        {
            enemyHealth = GetComponentInParent<EnemyHealth>();
            if(!enemyHealth)
            {
                print("enemyHealth is null (BodyPart)");
            }
        }

        public void Damage(int dmg)
        {
            dmg = (int)(dmg *multiplierDamage);
            enemyHealth.TakeDamage(dmg);
        }
    }
}
