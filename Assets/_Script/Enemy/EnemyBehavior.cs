﻿using UnityEngine;
using System.Collections;
using AR;

namespace AR
{
    //stany dostepne dla przeciwnika
    public enum enemyStateAllowed
    {
        Patrol,
        Chase,
        Atack,
        Guard,
        Investigate
    }

    abstract public class EnemyBehavior : EnemyBaseParam
    {
        //czas po jakim przeciwnik powinien zaczac szukac gracza
        public float timeToInvestigate = 1f;
        //czasz pilnowania podczas patrolu
        public float longGuard = 3;
        //czas po jakim ma szukac nowego punktu do patrolowania jesli stoimy w miejscu
        public float timeToFindNewPointPatrol = 2f;
        //liczba punktow do przeszukania podczas szukania gracza
        public int muchSearchPlayerInInvestigate = 4;
        //czy posiadamy pole do poszukiwan
        protected bool havePlaceToInvestigation;
        //srodek poszukiwan
        protected Vector3 searchPlace;

        protected EnemyMove enemyMove;
        protected EnemySight enemySight;
        protected EnemyMeleeAttack enemyMeleeAttack;

        protected enemyStateAllowed status;

        //ile juz pilnuje
        protected float timeGuard;
        //losowy czas na odpalenie animacji pilnowania
        protected float randomTimeGuard;
        //pozwalamy na jedno odpalenie animacji pilnowania
        protected bool guardRun;
        //akrualny czas przed poszukiwaniem gracza
        protected float timeInvestigate;
        //aktualny czas przed znalezieniem nowego punktu do patrolowania
        protected float timeToNewPointPatrol;

        //obecna ilosc przeszukanych punktow podczasz szukania gracza
        protected int currentInvestigatePoint;

        //pozycje dla sprawdzenia czy podczas patrolowania nie stoimy w miejscu
        protected Vector3 oldPositionRound;
        protected Vector3 newPositionRound;

        public enemyStateAllowed getState()
        {
            return this.status;
        }

        // Use this for initialization
        new protected void Start()
        {
            base.Start();

            this.oldPositionRound.x = Mathf.Round(this.transform.position.x);
            this.oldPositionRound.y = Mathf.Round(this.transform.position.y);
            this.oldPositionRound.z = Mathf.Round(this.transform.position.z);

            this.enemySight = GetComponentInChildren<EnemySight>();
            this.enemyMeleeAttack = GetComponent<EnemyMeleeAttack>();
            this.enemyMove = GetComponent<EnemyMove>();
            this.status = enemyStateAllowed.Guard;
            this.randomTimeGuard = 0;
            this.guardRun = false;
            this.currentInvestigatePoint = 0;
            this.havePlaceToInvestigation = false;
            this.timeToNewPointPatrol = 0;
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            ///print("status " + this.status);
            switch (this.status)
            {
                case enemyStateAllowed.Patrol:

                    this.Patrol();

                    break;

                case enemyStateAllowed.Guard:

                    this.Guard();

                    break;

                case enemyStateAllowed.Chase:

                    this.Chase();

                    break;

                case enemyStateAllowed.Atack:

                    this.Atack();

                    break;

                case enemyStateAllowed.Investigate:

                    this.Investigate();

                    break;
            }
        }

        abstract protected void Patrol();

        abstract protected void Guard();

        abstract protected void Chase();

        abstract protected void Atack();

        abstract protected void Investigate();
    }
}
